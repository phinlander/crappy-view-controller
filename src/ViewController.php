<?php
class ViewController{
	protected $params;

	function __construct(DTParams $params=null){
			$this->params = isset($params)?$params:new DTParams($_REQUEST);
	}
	
	public static function queryStringReplacing(array $params=array()){
		return http_build_query(array_merge($_REQUEST,$params));
	}
	
	public static function selfURLReplacing(array $params=array()){
		return $_SERVER["PHP_SELF"]."?".static::queryStringReplacing($params);
	}
	
	public static function appendParams($url, Array $params){
		$url_params = array();
		$parts = parse_url($url);
		if(isset($parts["query"]))
			parse_str($parts["query"],$url_params);
		return $parts["path"]."?".http_build_query($params+$url_params);
	}
}