<?php
class VCList extends ViewController{
	public $items;
	public $total;
	
	public function renderView($item_render_f){
		$html = "";
		$html .= "<div class='col-md-12'>";
		$html .= $this->renderPagination();
		$html .= "</div>";
		$html .= $this->renderEach($item_render_f);
		
		$html .= "<div class='col-md-12'>";
		$html .= $this->renderPagination();
		$html .= "</div>";
		return $html;
	}
	
	public function setItems($items){
		$this->items = $items;	
		$this->total = count($items);
	}
	
	public function requestItems($consumer,$action="list"){
		$pagination = $consumer->request($action,$this->params->allParams());
		$this->items = $pagination["items"];
		$this->total = $pagination["count"];
	}
	
	public function renderPagination(){
		$view = new VCPagination($this->params,count($this->items),$this->total);
		$html = "";
		$html .= $view->renderSummary();
		$html .= $view->renderPages();
		return $html;
	}
	
	public function renderEach($item_render_f){
		$html = "";
		foreach($this->items as $item){
			$html .= $item_render_f($item);
		}
		return $html;
	}
	
	public function renderSearch(){
		$html = "";
		$like_str = htmlentities($this->params->stringParam("like"));
		$html .= <<<END
		<form class="form">
			<fieldset>
				<legend>Search</legend>
				<div class='col-sm-8'>
					<div class='form-group'>
						<label for="like">Filter by Keyword</label>
						<input class="form-control" name="like" id="like" value="{$like_str}" placeholder="Keyword" />	
					</div>
				</div>
				<div class='col-sm-2'>
					<div class='form-group'>
						<label>&nbsp;</label>
						<input class='btn btn-primary form-control' type="submit" value="Filter" />
					</div>
				</div>
			</fieldset>
		</form>
END;
		return $html;
	}
	
	public function renderHeader($titles,$order_param_name="order",$desc_param_name="desc"){
		$_this = $this;
		$order = $this->params->stringParam("order");
		$desc = $this->params->stringParam("desc");
		return implode("", array_map(function($t) use ($order_param_name,$desc_param_name,$_this,$order,$desc){
			return "<th>".$_this->sortableTitle($t,$order,$desc,$order_param_name,$desc_param_name)."</th>";
		},$titles));
	}
	
	public function sortableTitle($name,$order,$desc,$order_param_name="order",$desc_param_name="desc"){
		$sort_icon = $desc?"glyphicon-chevron-down":"glyphicon-chevron-up";
		$sorting = ($order==$name?"<span class='glyphicon {$sort_icon}'></span>":"");
		$desc = ($order==$name?($desc=="1"?"0":"1"):"0");
		return "<a href='".htmlentities($this->selfURLReplacing(array($order_param_name=>$name,$desc_param_name=>$desc)))."'>{$name} {$sorting}</a>";
	}
}