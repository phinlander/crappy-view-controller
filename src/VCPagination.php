<?php
class VCPagination extends ViewController{
	protected $page;
	protected $count;
	protected $like;
	protected $item_count;
	protected $total;

	function __construct($params=null,$item_count,$total){
		parent::__construct($params);
		$this->page = $this->params->intParam("page",1);
		$this->count = $this->params->intParam("count",10);
		$this->like = $this->params->stringParam("like");
		$this->item_count = $item_count;
		$this->total = $total;
	}
	
	public function renderPages(){
		$page_num = $this->page;
		$per_page = $this->count;
		$total = $this->total;
	
		if($total==0)
			return;
		$start = ($page_num-1)*$per_page+1;
		$end = ($page_num-1)*$per_page+$total;
		$total_pages = ceil($total/$per_page);
		if($total_pages == 0)
			return;
		$params = $_REQUEST;
		$first_in_list = max(1,$page_num-5); //only go back 5
		$last_in_list = min($total_pages,$page_num+5); //only go forward 5
		$paging_html = "<nav><ul class='pagination'>";
		$params["page"] = 1;
		$link = http_build_query($params);
		$paging_html .= "<li><a href='".$_SERVER["PHP_SELF"]."?{$link}'>&laquo;</a></li>";
		$params["page"] = max(1,$page_num-1);
		$link = http_build_query($params);
		$paging_html .= "<li><a href='".$_SERVER["PHP_SELF"]."?{$link}'>&lsaquo;</a></li>";
		foreach(range($first_in_list,$last_in_list) as $i){
			$params["page"] = $i;
			$link = http_build_query($params);
		    $disabled = "";
		    if($i==$page_num)
		    	$disabled = "class='disabled'";
		    $paging_html .= "<li {$disabled}><a href='".$_SERVER["PHP_SELF"]."?{$link}'>{$i}</a></li>";
		}
		$params["page"] = min($total_pages,$page_num+1);
		$link = http_build_query($params);
		$paging_html .= "<li><a href='".$_SERVER["PHP_SELF"]."?{$link}'>&rsaquo;</a></li>";
		$params["page"] = $total_pages;
		$link = http_build_query($params);
		$paging_html .= "<li><a href='".$_SERVER["PHP_SELF"]."?{$link}'>&raquo;</a></li>";
		$paging_html .= "</ul></nav>";
		return $paging_html;
	}
	
	public function renderSummary(){
		$page_num = $this->page;
		$per_page = $this->count;
		$total = $this->total;
		$start = ($page_num-1)*$per_page+1;
		if($this->item_count==0) $start = 0;
		$end = ($page_num-1)*$per_page+$this->item_count;
		return "<h4 class='note pull-right'>Showing {$start}-{$end} of {$total}</h4>";
	}
}