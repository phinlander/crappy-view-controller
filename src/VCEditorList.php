<?php
class VCEditorList extends VCList{
	public $edit_url = "edit.php";
	public $edit_name = "Edit";
	public $new_params = array();
	public $remove_params = array();
	public $consumer_url = null;
	public $remove_action = "remove";
	public $header = array("Name");
	protected static $num_forms = 1;
	protected $form_id=null;

	protected function formID(){
		if(!isset($this->form_id))
			$this->form_id=static::$num_forms++;
		return "vc_editor_list_form_{$this->form_id}";
	}
	
	public function renderView($consumer_url,$tok,$each_f=null){
		$this->consumer_url = $consumer_url;
		$html = "";
		$html .= $this->renderPagination();
		$html .= $this->renderActionBar();
		$html .= $this->renderList($tok,$each_f);
		$html .= $this->renderActionBar();
		$html .= $this->renderPagination();
		
		?>
		<script>$(document).ready(function(){$("#select_all").click(function(){ $("input:checkbox[name='ids[]']").prop("checked",$("#select_all").prop("checked")); });});</script>
		<?php
		return $html;
	}
	
	public function renderCheckbox($id){
		return "<input type='checkbox' name='ids[]' value='{$id}' />";
	}
	
	public function renderEditButton($params){
		$edit_url = $this->appendParams($this->edit_url,$params);
		return "<button type='button' class='btn btn-default btn-xs' onclick='window.location.href=\"{$edit_url}\";'><span class='glyphicon glyphicon-edit'></span> {$this->edit_name}</button>";
	}
	
	public function renderList($tok,$each_f=null){
		$headerHTML = $this->renderHeader($this->header);
		if(!isset($each_f)){
			$_this = $this;
			$each_f = function($o) use ($_this){
				$checkbox_html = $_this->renderCheckbox($o["id"]);
				$edit_button_html = $_this->renderEditButton(array("id"=>$o["id"]));
				return "<tr><td>{$checkbox_html}</td><td>{$edit_button_html}</td><td>{$o["name"]}</td></tr>";
			};
		}
		$form_id = $this->formID();
		$html = <<<END
	<div class='col-md-12'>
		<form id='$form_id'>
			<input type="hidden" name="act" value="{$this->remove_action}" />
			<input type="hidden" name="tok" value="{$tok}" />
			<table class='table table-striped'>
				<thead><tr><th><input type="checkbox" id='select_all' /><th>{$this->edit_name}</th></th>{$headerHTML}</tr></thead>
				<tbody>
END;
		try{
			$html .= $this->renderEach($each_f);
		}catch(Exception $e){
			echo "<tr><td><div class='alert alert-warning' role='alert'>No results found.</div></td></tr>";
		}
		$remove_params_json = json_encode($this->remove_params);
		$html .= <<<END
				</tbody>
			</table>
		</form>
	</div>
	<script>function deleteSelected(){dt.post({"url":"{$this->consumer_url}","form":"#{$form_id}","data":{$remove_params_json},"success":function(){ window.location.reload(); }});}</script>
END;
		return $html;
	}
	
	public function renderPagination(){
		return "<div class='col-md-12'>".parent::renderPagination()."</div>";
	}
	
	public function renderActionBar(){
		return "<div class='col-md-12'>".$this->renderNewButton().$this->renderDeleteButton()."</div>";
	}
	
	public function renderNewButton(){
		$new_url = $this->appendParams($this->edit_url,$this->new_params);
		return "<button type='button' class='btn btn-success' onclick='window.location.href=\"{$new_url}\";'><span class='glyphicon glyphicon-plus'></span> New</button>";
	}
	
	public function renderDeleteButton(){
		$form_id = $this->formID();
		$html =<<<END
		<button type='button' class='btn btn-danger' data-toggle="modal" data-target="#delete_modal"><span class='glyphicon glyphicon-minus'></span> Remove</button>
		<div class="modal fade" id="delete_modal" tabindex="-1" role="dialog" aria-labelledby="DeleteModal" aria-hidden="true">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		        <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
		      </div>
		      <div class="modal-body">
		        Are you sure you wish to delete the selected items? This action cannot be undone easily.
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="deleteSelected()">Confirm</button>
		      </div>
		    </div>
		  </div>
		</div>
END;
		return $html;
	}
	
	public function setHeader($titles){
		$this->header = $titles;
	}
}