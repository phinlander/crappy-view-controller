<?php
class VCShareBar extends ViewController{
	function renderView(){
		$base_url = DTSettingsConfig::baseURL();
		$self = $base_url.$_SERVER["PHP_SELF"]."?".$_SERVER["QUERY_STRING"];
		$html =<<<END
		<a href="https://www.facebook.com/sharer.php?u={$self}" target="_blank"><img alt="Facebook" src="{$base_url}img/facebook.png" /></a> 
		<a href="https://twitter.com/share?url={$self}" target="_blank"><img alt="Twitter" src="{$base_url}img/twitter.png" /></a> 
		<a href="https://pinterest.com/pin/create/button/?url={$self}&amp;media={$base_url}apple-touch-icon.png" target="_blank"><img alt="Pinterest" src="{$base_url}img/pinterest.png" /></a> 
		<a href="https://plus.google.com/share?url={$self}" target="_blank"><img alt="Google+" src="{$base_url}img/google_plus.png" /></a> 
		<a href="https://www.linkedin.com/shareArticle?url={$self}" target="_blank"><img alt="LinkedIn" src="{$base_url}img/linkedin.png" /></a>
END;
		return $html;
	}
}